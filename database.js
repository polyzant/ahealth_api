var config = require('./config')[process.NODE_ENV]
, mongoose = require("mongoose")
, requireDir = require("require-dir")
, join = require('path').join
  var mongodb;
try{
  mongodb = mongoose.createConnection(config.mongoDatabase,function(err, db) {        
    if(err) {      
      console.log('Error! MongoDB connection error.........'+err);
      mongoose.connection.close();     
    }else{
      console.log('Success! MongoDB configuration success.........',config.mongoDatabase);
     }
  });
}catch(e) {
  console.log('Exception....');
  console.log(e);
}
var Productsmodels = requireDir(join(__dirname, "./models"));
var Products = mongoose.model('Products',Productsmodels);
module.exports = mongodb;