var config = require('../../config')[process.NODE_ENV]
, mongodb = require('../../database')
, Products = mongodb.model('Products')
module.exports = function(req, res) {  
	Products.find({}, function (err, products) {
		
	    if (err){
	      res.json({"code": 500, "error": "Error in fetching Products list."});      
	    } else {	    	  
	      console.log(products.length + ' products were fetched from mongo ')	      
	      res.json({ "total": products.length, "items": products });
	    }
  	});
}