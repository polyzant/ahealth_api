var express = require('express')
  , path = require("path")
  , http = require('http')
  , bodyParser = require('body-parser')
  
var app = express()
  , app_root = __dirname
  
process.NODE_ENV = app.get('env')
var config = require('./config')[process.NODE_ENV]
, mongodb = require('./database');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

checkAuthorization = function(req, res, next){
  res.send(200);
};

app.get('/', require('./src/home/home'));
app.get('/products',require('./src/products/getAll'));

app.listen(config.port, () => console.log('Server listening on port '+ config.port) );

