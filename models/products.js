var Schema,Products;
Schema = require('mongoose').Schema;
Products = new Schema({
  id: {
    type: Number
  },
  txntid : {
    type: Number
  },
  username : {
    type: String,
    trim: true
  },
  productzone: {
    type: String,
    trim: true
  },
  product: {
    type: String,
    trim: true
  },
  brand: {
    type: String,
    trim: true
  },
  model: {
    type: String,
    trim: true
  },
  datetime: {
    type: Date,
    "default": Date.now
  },
  quantity:{
    type:Number,
    required: true
  },
  quantity:{
    type:Number,
    required: true
  }
  
});
module.exports = Products;